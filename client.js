'use strict';
const babylon = require('babylonjs');

const canvasElement = document.getElementById('render-canvas');
document.body.classList.add('loaded');

const engine = new babylon.Engine(canvasElement, true);

const createScene = function (engine) {
    const scene = new babylon.Scene(engine);
    scene.clearColor = new babylon.Color3(0, .5, 1);

    const camera = new babylon.FreeCamera('camera1', new babylon.Vector3(0, 5, -10), scene);
    camera.setTarget(babylon.Vector3.Zero());
    camera.attachControl(engine.getRenderingCanvas(), false);

    const light = new babylon.HemisphericLight('light1', new babylon.Vector3(0, 1, 0), scene);
    light.intensity = 0.75;

    const sphere = babylon.Mesh.CreateSphere('sphere1', 16, 2, scene);
    sphere.position.y = 0.75;

    return scene;
};

const scene = createScene(engine);

engine.runRenderLoop(() => scene.render());
window.addEventListener('resize', () => engine.resize());
