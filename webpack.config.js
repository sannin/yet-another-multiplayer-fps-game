'use strict';
module.exports = {
  entry: './client.js',
  output: {
    path: `${__dirname}/build/webpack`,
    filename: 'client.js',
  },
};
