'use strict';

const express = require('express');

const app = express();

const staticConfig = {
  extensions: [
    'xhtml',
  ],
  index: 'index.xhtml',
};

for (let dir of [ 'public', 'build/webpack', ]) {
  app.use(express.static(dir, staticConfig));
}

module.exports = app;
